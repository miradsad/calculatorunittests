public class Calculator {
    public enum OPERATION {
        SUM, SUBTRACT, MULTIPLY, DIVIDE;
    }
    public double calculate(OPERATION operator, double num1, double num2) {
        double result = 0;
        try {
            switch (operator) {
                case SUM -> result = sum(num1, num2);
                case SUBTRACT -> result = subtract(num1, num2);
                case MULTIPLY -> result = multiply(num1, num2);
                case DIVIDE -> result = divide(num1, num2);
                default -> {
                    System.out.println("Ошибка! Введена некорректная операция.");
                    System.exit(0);
                }
            }
        } catch (ArithmeticException e) {
            System.out.println("Ошибка! " + e.getMessage());
            System.exit(0);
        }
        return result;
    }
    public double sum(double num1, double num2) {
        return num1 + num2;
    }
    public double subtract(double num1, double num2) {
        return num1 - num2;
    }

    public double multiply(double num1, double num2) {
        return num1 * num2;
    }

    public double divide(double num1, double num2) {
        if (num2 == 0) {
            throw new ArithmeticException("На 0 делить нельзя!");
        }
        return num1 / num2;
    }
}