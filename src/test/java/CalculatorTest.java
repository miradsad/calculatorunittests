import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
public class CalculatorTest {

    @ParameterizedTest
    @EnumSource(Calculator.OPERATION.class)
    public void testCalculate(Calculator.OPERATION operator) {
        Calculator calculator = new Calculator();

        double num1 = 10;
        double num2 = 5;
        double expected = 0;

        switch (operator) {
            case SUM -> expected = calculator.sum(num1, num2);
            case SUBTRACT -> expected = calculator.subtract(num1, num2);
            case MULTIPLY -> expected = calculator.multiply(num1, num2);
            case DIVIDE -> expected = calculator.divide(num1, num2);
        }
        double result = calculator.calculate(operator, num1, num2);
        Assertions.assertEquals(expected, result);
    }
}